﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Model.Model
{
    public class PickupLocation
    {
        public int pickup_location_id { get; set; }
        public string location { get; set; }
        public string lattitude { get; set; }
        public string longitude { get; set; }
        public string created_by { get; set; }
        public DateTime? created_on { get; set; }
    }
}
