﻿using Microsoft.Extensions.Configuration;
using Npgsql;
using ShuttleBookingService.DataAccess.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace ShuttleBookingService.DataAccess.CommandExecuter
{
    public class Executers : IExecuters
    {
        private readonly IDbConnection _dbConnection;

        public Executers(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public void ExecuteCommand1(Action<NpgsqlConnection> task)
        {
            var conn = new NpgsqlConnection(_dbConnection.ConnectionString);
            try
            {
                  
                    //conn.Open();
                    task(conn);
                   // conn.Close();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
             //    conn.Close();
            }
        }

        public T ExecuteCommand<T>(Func<NpgsqlConnection, T> task)
        {
            NpgsqlConnection conn = new NpgsqlConnection();
            try
            {
                //using (NpgsqlConnection conn = new NpgsqlConnection(_dbConnection.ConnectionString))
                //{
                    conn = new NpgsqlConnection(_dbConnection.ConnectionString);
                   // conn.Open();
                    return task(conn);

                //}
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally {
               // conn.Close();
            }
           
            
        }
    }
}
