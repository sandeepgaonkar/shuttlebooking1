﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.DataAccess.CommandExecuter
{
    public interface IExecuters
    {
       // void ExecuteCommand(Action<NpgsqlConnection> task);
        T ExecuteCommand<T>(Func<NpgsqlConnection, T> task);

        //T ExecuteCommandOutput(Action<NpgsqlConnection> task);
    }
}
