﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.DataAccess.Queries
{
    public class CommandText : ICommandText
    {
       
        public string GetAllTransportRequests => " select *,st.status_name,lp.location as pickup_location from public.transport_request" +
            " tr join public.status st on tr.status_id= st.status_id join library_pickup_location lp on tr.pickup_location_id = lp.pickup_location_id where  DATE(tr.created_on) = now()::date";
        public string GetTransportRequestById => "select *,st.status_name,lp.location as pickup_location from public.transport_request" +
            " tr join public.status st on tr.status_id= st.status_id join library_pickup_location lp on tr.pickup_location_id = lp.pickup_location_id where  tr.request_id = @request_id";
        public string GetTransportRequestByempId => "select *,st.status_name,lp.location as pickup_location from public.transport_request" +
           " tr join public.status st on tr.status_id= st.status_id join library_pickup_location lp on tr.pickup_location_id = lp.pickup_location_id where  tr.employee_id = @request_id order by request_id desc limit 1";

        public string GetTransportRequestByManagerId => "select *,st.status_name,lp.location as pickup_location from public.transport_request" +
            " tr join public.status st on tr.status_id= st.status_id join library_pickup_location lp on tr.pickup_location_id = lp.pickup_location_id where   DATE(tr.created_on) = now()::date and ( manager_id = @managerId or manager_manager_id = @managerManagerId)";

        public string AddTransportRequest => "INSERT INTO public.\"transport_request\"(employee_id, contact_phone, account, email, request_reason, pickup_location_id, to_location, to_lattitude, to_longitude, pin_code, no_of_passengers, manager_id, manager_manager_id, request_time, request_date, created_on, status_id)" +
            "VALUES(@employee_id, @contact_phone, @account, @email, @request_reason, @pickup_location_id, @to_location, @to_lattitude, @to_longitude, @pin_code, @no_of_passengers, @manager_id, @manager_manager_id, @request_time, @request_date,NOW(),1)"+
            "RETURNING request_id";

        public string AddEmployeeDetails => "UPDATE public.\"employee\" SET phone_no= @phone_no , emergency_contact= @emergency_contact, email= @email, " +
            "account= @account, project= @project,practice_area= @practice_area, manager_id= @manager_id, manager_name= @manager_name, manager_username= @manager_username," +
            "manager_email= @manager_email, manager_phone_no= @manager_phone_no, manage_emergency_contact= @manage_emergency_contact," +
            "manager_manager_id= @manager_manager_id, manager_manager_name = @manager_manager_name, manager_manager_username= @manager_manager_username, " +
            "manager_manager_email= @manager_manager_email, manager_manager_phone_no= @manager_manager_phone_no, manager_manager_emergency_contact= @manager_manager_emergency_contact, modified_on= NOW() " +
            "where employee_id = @employee_id ;" +
            "INSERT INTO public.\"employee\"(employee_id, employee_name, employee_username, gender, phone_no, emergency_contact, email, account, " +
            "project, practice_area, manager_id, manager_name, manager_username, manager_email, manager_phone_no, manage_emergency_contact, manager_manager_id, " +
            "manager_manager_name, manager_manager_username, manager_manager_email, manager_manager_phone_no, manager_manager_emergency_contact, " +
            "created_on,active) SELECT " +
            "@employee_id,@employee_name, @employee_username, @gender, @phone_no, @emergency_contact, @email, @account, @project," +
            "@practice_area, @manager_id, @manager_name, @manager_username, @manager_email, @manager_phone_no, @manage_emergency_contact," +
            "@manager_manager_id, @manager_manager_name, @manager_manager_username, @manager_manager_email, @manager_manager_phone_no," +
            "@manager_manager_emergency_contact, NOW(), '1' WHERE NOT EXISTS(SELECT 1 FROM public.\"employee\" WHERE employee_id=@employee_id)";

        public string GetAllGetPickupLocations => "select * from public.\"library_pickup_location\"";
        public string GetAllVendors => "select * from public.\"vendor_master\"";

        public string ApproveByManager => "update public.transport_request  set " +
            "approved_by = @ApproveBy," +
            "approved_on= NOW() where request_id = @request_id";

        /*public string ApproveByManagerManager => "update public.\"transport_request\"  set " +
           "approved_by = (select manager_manager_name from public.\"employee\" where employee_id = @employee_id)," +
           "approved_on= NOW() where request_id = @request_id";*/
        public string AllocateShuttle => "update public.\"transport_request\"  set vendor_id = @vendorId::int4 where request_id = @requestId";
    }
}
