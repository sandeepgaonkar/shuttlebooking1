﻿using Dapper;
using ShuttleBookingService.DataAccess.CommandExecuter;
using ShuttleBookingService.DataAccess.Queries;
using ShuttleBookingService.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Business.Repository
{
    public class PickupLocationManager : IPickupLocationManager
    {
        private readonly ICommandText _commandText;
        private readonly IExecuters _executers;
        public PickupLocationManager(ICommandText commandText, IExecuters executers)
        {
            _commandText = commandText;
            _executers = executers;
        }
        public async Task<List<PickupLocation>> GetPickupLocations()
        {
            try
            {
                var query = await _executers.ExecuteCommand(
                  conn => conn.QueryAsync<PickupLocation>(_commandText.GetAllGetPickupLocations));
                return query.ToList();
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
