﻿using Dapper;
using ShuttleBookingService.DataAccess.CommandExecuter;
using ShuttleBookingService.DataAccess.Queries;
using ShuttleBookingService.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Business.Repository
{
    public class ManageTransportRequests : IManageTransportRequests
    {

        // private readonly IConfiguration _configuration;
        private readonly ICommandText _commandText;
        private readonly IExecuters _executers;
        public ManageTransportRequests(ICommandText commandText, IExecuters executers)
        {
            _commandText = commandText;
            //_configuration = configuration;
            _executers = executers;
        }
       
        public async Task<List<TransportRequest>> GetTransportRequestByManagerId(string managerId)
        {
            try
            {
                var _requestId = await _executers.ExecuteCommand(
                   conn => conn.QueryAsync<TransportRequest>(_commandText.GetTransportRequestById, new
                   {
                       @managerId = managerId,
                       @managerManagerId = managerId,
                   }));

                return _requestId.ToList();
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> ApproveRequestByManager(int RequestID, string ApproveBy)
        {
            try
            {
                    var _requestId = await _executers.ExecuteCommand(
                  conn => conn.QueryAsync<TransportRequest>(_commandText.ApproveByManager, new
                  {
                      @ApproveBy = ApproveBy,
                      @requestId = RequestID,
                  }));
                /*else
                {
                    var _requestId = await _executers.ExecuteCommand(
                  conn => conn.QueryAsync<TransportRequest>(_commandText.ApproveByManagerManager, new
                  {
                      @employeeId = entity.employee_id,
                      @requestId = entity.request_id,
                  }));
                }*/
                return true;
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<bool> AllocateShuttle(int RequestID, int VendorId)
        {
            try
            {
                    var _requestId = await _executers.ExecuteCommand(
                  conn => conn.QueryAsync<TransportRequest>(_commandText.AllocateShuttle, new
                  {
                      @vendorId = VendorId,
                      @requestId = RequestID,
                  }));
                    return true;
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
