﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ShuttleBookingService.Business.Repository;
using ShuttleBookingService.Model.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ShuttleBookingService.Controllers
{
    
    [ApiController]
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    public class VendorController : Controller
    {
        private readonly IVendorManager _vendorManager;
        public VendorController(IVendorManager vendorManager)
        {
            _vendorManager = vendorManager;
        }
        // GET: api/<controller>
        [HttpGet]        
        public async Task<IActionResult> Get()
        {
            try
            {
                var vendorDetails = await _vendorManager.GetVendors();
                var apiResponse = new ApiResponse();
                if (vendorDetails != null)
                {
                    
                    apiResponse.message = "Find all vendors deatils";
                    apiResponse.status = "Success";
                    apiResponse.data.Add(vendorDetails);
                    return Ok(apiResponse);
                }
                else
                {
                    apiResponse.message = "Vendors deatils not found";
                    apiResponse.status = "Not Found";
                    return NotFound(apiResponse);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }

        }

        

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
