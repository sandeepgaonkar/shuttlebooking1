﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ShuttleBookingService.Business.Repository;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ShuttleBookingService.Controllers
{
   
    [ApiController]
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    public class PickupLocationController : Controller
    {
        private readonly IPickupLocationManager _pickupLocationManager;
        public PickupLocationController(IPickupLocationManager pickupLocationManager)
        {
            _pickupLocationManager = pickupLocationManager;
        }
        // GET: api/<controller>
        [HttpGet]   
        public async Task<IActionResult> GetPickupLocations()
        {
            try
            {
                var pickupLocations = await _pickupLocationManager.GetPickupLocations();
                var apiResponse = new ApiResponse();
                if(pickupLocations != null)
                {
                    apiResponse.message = "Find all pickup locations";
                    apiResponse.status = "Success";
                    apiResponse.data.Add(pickupLocations);
                    return Ok(apiResponse);
                }
                else
                {
                    apiResponse.message = "Pickup locations not found";
                    apiResponse.status = "Not Found";
                    return NotFound(apiResponse);
                }
                
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }

        }

       

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
