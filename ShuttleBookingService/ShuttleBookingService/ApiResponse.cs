﻿using ShuttleBookingService.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService
{
  
    public class ApiResponse
    {
        public string message { get; set; }
        public string status { get; set; }
        //public object data;
        public List<object> data = new List<object>();
        //public List<ShuttleBookingResponse> RequestDetails = new List<ShuttleBookingResponse>();
    }

    public class ShuttleBookingResponse
    {

        public int request_id ;
        public string employee_id;
        public string contact_phone;
        public string account;
        public string email;
        public string request_reason;
        public int pickup_location_id;
        public string pickup_location;
        public string to_location;
        public string pin_code;
        public int no_of_passengers;
        public int vendor_id;
        public string total_expense;
        public string manager_id;
        public string manager_manager_id;
        public string approved_by;
        public DateTime? approved_on;
        public TimeSpan request_time;
        public DateTime request_date;
        public string request_date_time;
        public DateTime created_on;
        public int status_id;
        public string status_name;
        public string cancelled_by;
        public DateTime? cancelled_on;
        public string comment;
        public string employee_name;
    }
}
