import React, { Component } from 'react';


var TransportRequestData = (function() {
   var request_id = "";
   var status = "";
  
  var getRequestId = function() {
      return request_id;    // Or pull this from cookie/localStorage
    };
    

  var setRequestId = function(id) {
      request_id = id;     
      // Also set this in cookie/localStorage
    };

    var getStatus = function() {
      return request_id;    // Or pull this from cookie/localStorage
    };
    

  var setStatus = function(status) {
      status = status;     
      // Also set this in cookie/localStorage
    };
  return {
    getRequestId: getRequestId,
    setRequestId: setRequestId,
    getStatus : getStatus,
    setStatus : setStatus
  }

})();

export default TransportRequestData;


