import React from 'react';
import axios from 'axios';
import { AgGridReact } from 'ag-grid-react';
import {Button} from 'react-bootstrap';
import '../App.css';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import DateBetween from './DateBetween';
import ChildMessageRenderer from './ChildMessageRenderer';
import { appConfig } from '../Configuration/config.js';
import { Layout } from './Layout';
import TransportRequestData from './TransportRequestData';
import moment from 'moment'



export default class adminView extends React.Component {  
  baseUrl = appConfig.trasportApi;
  constructor(props) {
    super(props);
  this.state = {
    selectedRows : '',
    columnDefs: [
      {headerName: 'Request '+' Id', field: 'request_id', sortable: true, filter: true}, 
      {headerName: 'Employ '+' Id', field: 'employee_id', sortable: true, filter: true},
      {headerName: 'Email', field: 'email', sortable: true, filter: true},
      {headerName: 'Date', field: 'request_date', valueFormatter: function (params) {
        return moment(params.value).format('DD-MM-yyyy');
   }
       , sortable: true, filter: true},
       {headerName: 'Time', field: 'request_time', sortable: true, filter: true},
      { headerName: 'Request Date',
        colId: 'request_date & request_time',
        valueGetter: function(params) {
          return params.data.request_date + " "+ params.data.request_time;
        },
      },
      {headerName: 'Request ' + ' Reason', field: 'request_reason', sortable: true, filter: true},
      {headerName: 'Pickup'+ ''+'Location', field: 'pickup_location', sortable: true, filter: true},
      {headerName: 'Drop'+'' +'Location', field: 'to_location', sortable: true, filter: true},
      {headerName: 'Approved By', field: 'approval_reason', sortable: true, filter: true},
      {headerName: 'Approval Status', field: 'status_name',  sortable: true, filter: true},    
      {headerName: 'Cab Status', field: 'cabstatus', cellRenderer: 'ChildMessageRenderer',
      colId: 'params'}
    ],
    requestList: '',
    context: { componentParent: this,
          },
      frameworkComponents: {      
        ChildMessageRenderer: ChildMessageRenderer,
      },  
      onSelectionChanged: this.onSelectionChanged, 
      gridOption : {
        getRowNodeId: function(data) {
          return data.id;
        }   
      }
  };
}

 onSelectionChanged = (e) => {
   debugger;
  var selectedRows = e.api.getSelectedRows();
  var request_id =  selectedRows.length === 1 ? selectedRows[0].request_id : '';
  var status = selectedRows.length === 1 ? selectedRows[0].status_name : '';
  console.log("Selected Requestid = ", request_id)
  console.log("Selected status = ", status)
  TransportRequestData.setRequestId(request_id);  
  TransportRequestData.setStatus(status)
};

  methodFromParent = cell => {
    alert('Parent Component Method from ' + cell + '!');
  };  

getTransportRequestDetails=() => {
  axios.get(this.baseUrl)
  .then(res => {
    const items = res.data.data;
    console.log("AdminView Request List",items)    
    this.setState({requestList:items});
    return this.requestList;
  })
}

  componentDidMount() {
    this.getTransportRequestDetails();
  }
  render() {
    return (      
      <div className="ag-theme-alpine">
        <Layout/>  
        <br/>
        <AgGridReact 
        containerStyle={{       
        height: '400px'}}
        columnDefs={this.state.columnDefs}
        rowData={this.state.requestList}
        rowSelection="single"    
        onSelectionChanged={this.state.onSelectionChanged}
        context={this.state.context}
        frameworkComponents={this.state.frameworkComponents}
        pagination='true'
        paginationPageSize= '20'
    />                     
      </div>
    )
  }
}