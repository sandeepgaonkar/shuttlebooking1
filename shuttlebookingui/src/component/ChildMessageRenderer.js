import React, { Component } from 'react';
import axios from 'axios';
import { appConfig } from '../Configuration/config.js';
import { Modal } from "react-responsive-modal";
import {Button} from 'react-bootstrap';
import "react-responsive-modal/styles.css";
import adminView from './adminView.js';
import swal from 'sweetalert';
import TransportRequestData from './TransportRequestData';
import { myConfig } from './Config.js'

const styles = {
  fontFamily: "sans-serif",
  textAlign: "center"
};

export default class ChildMessageRenderer extends Component {
  baseURL = myConfig.apiUrl;
  
  constructor(props) {
    super(props);
    this.state = {
        value: props.value,
        Showmodal : false,
        disabled : true,
        list : props.requestList,
        open: false,
        vehical : '',
        vehicalList : [],
        text : false,
        requestId : '',
        status : ''
      };
    
    this.handleChange = this.handleChange.bind(this); 
    this.submit = this.submit.bind(this);
    this.Callback();
  } 

  handleChange = (event) => {
    this.setState({vehical : event.target.value});
  };

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
    this.refresh();
  };
  refresh()
  {
    this.setState({text:true})
  }
 
   Callback(){
    var req_id = TransportRequestData.getRequestId();
    this.setState({requestId : req_id});

    var statusname = TransportRequestData.getStatus();
    this.setState({status : statusname});
  }
 
  componentDidMount()
  {
    axios.get(this.baseURL+"Vendor")
      .then(res => {
         const items = res.data.data[0];
         this.setState({vehicalList : items})   
         this.Callback();      
  })       
    
  } 
  
  submit = async (event) => {
    debugger;
    
    axios.post(this.baseURL+"managerequest/AllocateShuttle", {
           headers: {
              "Content-Type": "application/json; charset=utf-8",
               },
      body : JSON.stringify({
        request_id : document.getElementById('requestId').value,
        vendor_id : document.getElementById('vendorId').value         
          
      })
  })
  .then(response => {
    console.log("Response Data :", response.data);
    var result = response.data.data;

    console.log('post status', result)  
    console.log("Event : ", event)
    })
  }

  render() {       
    const { open } = this.state; 
    var check = this.state.text===false ?this.state.vehical : '' 
    console.log("RequestId from TransportRequest : ", this.state.requestId); 
    let options = this.state.vehicalList.map((data) =>
                 <option 
                     key={data.vendor_id}
                     value={data.vendor_id}                 >
                     {data.vehicle_number}
                 </option>
             );     
       
     return (
       <div style={styles}>
         
         <Button
         onClick={this.onOpenModal}
        >
         Unassigned
         </Button>
         <Modal open={open} onClose={this.onCloseModal}>        
         <h3>Available Cab Lists</h3>      

         <form>      
          <span>Request Id</span>
          <div >          
          <input type="text" id = "requestId" className="form-control" value = {this.state.requestId} disabled name="requestid" 
          />
          </div>
          <div >     
           Cab      
          <select id = "vendorId" className="form-control" >
          <option>Select Cab</option>
          {options}
          </select>  
          </div> 
          <div>
          <Button
          variant="outline-success"  onClick= {this.submit}>    
          Assign Cab        
          </Button>  
          </div>                       
          </form> 
       </Modal>      
       </div> 
     );
   }
  }

