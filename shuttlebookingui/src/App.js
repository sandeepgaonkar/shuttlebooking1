import React, { Component } from 'react';
import { Route, Switch} from 'react-router';
import Login from './component/Login';
import TabRoutes from './component/TabRoutes';
import adminView from './component/adminView';

export default class App extends Component {
  displayName = App.name
  

  render() 
  {
    return(  
     <div className="root">
        <Switch>       
        <Route exact path="/" component={Login} />         
        <Route exact path="/adminView" component={adminView} />
        <Route exact path="/tabs" component={TabRoutes} />    
        
        </Switch>
        
      </div>
    );
  }
}
