import React,{useEffect} from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import {
  BrowserRouter,
  Switch,
  Route,
  Link
} from "react-router-dom";
import GetRequest from '../component/GetRequest';
import CabRequest from '../component/CabRequest';


function TabPanel(props) {
    const { children, value, index, ...other } = props;
    console.log(props)
   
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  
  function a11yProps(index) {
    console.log("allyprops",index)
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }
  
  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
    },
  }));

  export default function SimpleTabs() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const handleChange = ( newValue) => {
      setValue(newValue);
    };
    
    return ( 
      <div className={classes.root}>
        <BrowserRouter>
        <AppBar position="static">
          <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
            <Tab label="Request eCabs" component={Link} to="/request" {...a11yProps(0)}/>
            <Tab label="eCab Status" component={Link} to="/eCabStatus" {...a11yProps(1)} />
          </Tabs>
        </AppBar>
        <TabPanel>
          {window.location.pathname==="/tabs"&&(<CabRequest/>)}
      </TabPanel>
      <Switch>
           <Route exact path="/eCabStatus"  component={GetRequest} />
           <Route exact path="/request" component={CabRequest}/>
      </Switch> 
        </BrowserRouter> 

      </div> 
    );
  }